/******************************************************************************
Dos computadores en una misma red local, suelen tener direccion IP similares.
En Pastry, una vez que el nodo raiz obtiene la copia del dato, el nodo raiz,
se encarga de transferir el dato a todo su LeafSet. Lo cual, a pesar de ser
costoso, permite tener altamente disponible el dato (información), dadas las
propiedades del LeafSet.

Route(mensaje, Hash) : Retorna => Nodo cuyo node_id es el más cercano al hash.
******************************************************************************/

/******************************************************************************
SIGUIENDO CON PASTRY:
En caso de que uno de los nodos del LeafSet se desconecta, la condicion de la
cantidad de LeafSets se debe mantener, por lo tanto, añade el siguiente nodo 
más cercano. Por tanto, el nodo que debe REPARAR sus LeafSet, consulta con los
nodos adjuntos, que nodo debe agregar a su LeafSet.
EJ:
1.  Cadena de Hash Original, con L = 2 y nodo de interes X 
    H <-> G <-> F <-> E <-> X <-> A <-> B <-> C <-> D
    Se tiene que el LeafSet de X es: F,E,A,B
2.  Si A se retira (envía un mensaje "leave") la cadena queda como:
    H <-> G <-> F <-> E <-> X <-> B <-> C <-> D
    Se tendrá entonces, que el LeafSet de X es: F,E,B,C
3.  En caso de que A, se retira sin avisar (por fallo o comportamiento mal 
    intencionado), se hace una consulta frecuente de estado (como en las
    redes locales, como se comporta el NAT mediante DHCP, algo así como
    el lease_time en un Router).
    Por lo tanto, cada cierto tiempo (delta_t), el nodo raiz, enviará un
    mensaje "keep_alive"

El valor de delta_t no debe ser muy pequeño, ya que inundaria la red, porque 
el costo es muy alto (alta carga de red), pero hace la red muy reactiva a 
cambios en los nodos.
Por el contrario, un delta_t muy alto, el costo es bajo, pero la red no es
reactiva ante cambios de la red.

En el caso de que vuelva el nodo que se retiró (voluntaria o involuntariamente)
o ingrese un nuevo nodo, este debe avisar, a todos los nodos adjuntos que se
encuentren dentro de su leafSet. El nodo adjunto raiz, conoce entonces el
LeafSet de el nuevo nodo. Dado que el nodo raiz conoce el node_id de todo su
LeafSet y el nodo nuevo tiene un node_id dentro del rango de valores del
LeafSet del nodo raiz, el nodo raiz, asume que el nuevo nodo debe ser parte de
LeafSet.
El algoritmo de ruteo permite balancear la carga de la red, al
consultar/obtener la información/dato. Ya que al realizar el ruteo, se
garantiza que la ruta obtenida, pase por el LeafSet del nodo raiz.
******************************************************************************/

#include <stdio.h>

void main(void){
    printf("Notas del día 08 de Octubre de 2019 de Computación Peer to Peer, ver archivo fuente.\n");
}