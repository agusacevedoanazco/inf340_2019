/******************************************************************************
Cuando un nuevo nodo entra, debe construir su LeafSet, lo cual es sencillo,
sabiendo el LeafSet de los nodos adjuntos (vecinos). Esto suponiendo que
ningun nodo tiene comportamiento malicioso (ninguno miente o tiene intenciones
de generar errores en la red).
El numero de saltos es O(log(N)), por lo cual se dice que el sistema es
escalable, ya que aumentando la cantidad de nodos, el numero de saltos NO es
"demencialmente alto".
El penultimo salto siempre pasa por uno de los nodos dentro del LeafSet del
nodo de interés.
No todos los ruteos entran por el mismo nodo del LeafSet, por lo cual, si
muchos clientes están consultando por la misma información, permite que se
distribuya la carga de datos.
Cuando un nodo se retira del LeafSet (leave message), los encargados de
presentar al nuevo nodo del LeafSet del nodo raiz, son los nodos del mismo
LeafSet. Esto es considerando que el nodo avisa (genera el mensaje de retiro).
Para reconocer que un nodo se retira sin avisar, el nodo raiz, está encargado
de enviar un mensaje "keep-alive" (algo asi como un "ping" sobre la capa
de aplicación), para reconocer que sus nodos se encuentran aun activos.
Se le conoce como fase de Startup, como la fase de tiempo suficiente para que
el sistema contenga la cantidad necesaria de nodos, de modo de obtener las
propiedades de un sistema p2p, es decir, alta disponibilidad de los datos.
A medida que va aumentando la cantidad de nodos, se van modificando el LeafSet
del nodo raiz, lo cual genera que, los nodos iniciales del LeafSet dejen de
ser parte del LeafSet. Por lo cual, se encuentra la posibilidad que, los nodos
iniciales del LeafSet, vuelvan a ser parte del LeafSet, pero por el "otro lado"
del LeafSet.
The Neighborhood Set saves the nodes that have lower latency between them and
the root node
******************************************************************************/

/******************************************************************************
MSB: Most significative bit
Si quiero rutear de 10233102 a 10311203, el prefijo más grande entre los nodos
es: 10 (MSB constantes, en este caso MSQ constantes)
El proceso sería:
10233102 -> 103XXXXX -> 1031XXXX -> 10311XXX -> 103112XX -> 1031120X -> destino
Entonces el proceso debiese ser aumentando la cantidad de MSB. Es por ello, que
los saltos son de orden logaritmico, ya que, a medida que se genera un salto,
quedan la mitad de nodos por saltar. Por lo tanto la distancia de cada salto
se puede traducir en: T(n) = T(n/2) + K y por arbol recursivo, se tiene que el
orden del algoritmo es O(log(N))
En el penultimo salto, se llega a un nodo del LeafSet del nodo raiz, este nodo,
no guarda en su tabla de ruteo un nodo con un prefijo con más bits que el
salto recien realizado, por lo cual debe revisar su tabla de ruteo, para llegar
al nodo con el NodeId más cercano al Hash del dato.
******************************************************************************/

/******************************************************************************
Pastry Routing Algorithm
    Input: Message, Key
    Si el nodo está dentro del LeafSet, se hace forward al nodo con menor
    distancia de key.
    ...
******************************************************************************/

/******************************************************************************
En el caso de que el nodo al cual se hace forward no existe, dado que el nodo
ya no se encuentra disponible, se informa que el nodo ya no existe. Por lo
tanto, solamente cuando el nodo desaparece y quisiera ser utilizado, se avisa
a todos los nodos que necesitan rutear por el nodo que no se encuentra
disponible.
Para reparar, se consulta con los nodos de la misma linea en la tabla de ruteo,
ya que esos nodos, tienen la discordancia de bits en el mismo lugar (en el MSB)
por lo cual, debiesen tener información para ese ruteo.
******************************************************************************/
void main(void){
    printf("Notes from October 10th\n");
}