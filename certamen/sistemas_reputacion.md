# Sistemas de reputacion

En la realidad, las redes contienen clientes que tienen comportamientos no deseados; es decir, no siguen los protocolos de la red: no rutean, no contestan, tienen comportamientos maliciosos, etc.
Los sistemas de reputacion, buscan evaluar la calidad del usuario, para mitigar el efecto de estos nodos.

## Sistema de Accountability

COntiene testigos de las transacciones entre nodos, que certifican que la respuesta de un cliente es correcta y acorde a lo esperado.
Necesita que todos los nodos de la red sean capaces de hacer el mismo algoritmo que la transaccion, por lo tanto, es costoso y poco practico.
Es muy confiable.

## Sistemas de reputacion funcionamiento

A cada nodo se le asocia una reputacion "R(x)" en el intervalo 0 y 1 (intervalo abierto).
R(x) := probabilidad de que un nodo x sea confiable
1 - R(x) : = probabilidad de que x sea malicioso

### Reputacion

Luego de una transacción, cada nodo emite una recomendacion sobre el nodo.
F(t,A)(X) := Evaluacion del nodo A acerca de la transaccion en el tiempo t, acerca del nodo X.
Los valores que pueden tomar la evaluacion son discretos

|valor|opinion|
|-----|-------|
|1|Excelente|
|0.75|Buena|
|0.5|Neutral|
|0.25|Mala|
|0|Maliciosa|

Cada nodo tiene un manager de reputación, el cual recibe la recomendacion de su nodo evaluado, donde los manager corresponderan a: hash_manager = SHA(nodeID(X)), manager del nodo X.
Los manger mantienen en una lista, las opiniones de sus nodos y calcularán la reputacion de estos respecto a la credibilidad de los que envían la opinion.

La credibilidad del nodo K en el tiempo t (C(k,t)) dependerá de igual forma de su reputacion.

|Credibilidad|Rango Reputacion|
|------------|----------------|
|1|0.75->1|
|0.75|0.5->0.75|
|0.2|0.25->0.5|
|0.1|0->0.25|

### Calculo de la reputacion

La reputacion corresponderá a

```pseudo
Reputacion(x) = sumatoria(i=0->m-1)[log(m-1+1)*recomendacion(i,K,X)*credibilidad(k,t)]/sumatoria(i=1->m-1[log(m-i+1)*credibilidad(k,t)])
// Por tanto, las evaluaciones mas recientes, ponderarán en mayor medida la reputacion que las más antiguas
```

La reputación de un nodo entrante, suele asignarse en 0.7, de modo de asegurar que el nodo pueda hacer transacciones.

### Uso de los calculos anteriores

Cuando un nodo recibe una peticion de X, el obtiene la reputacion de los manager de X y determina la reputacion respecto a la moda de valores de reputación. En caso de que la decision sea, no realizar la transaccion, se cambia de nodo (diversity routing).

### Calculo de riesgo

Es realmente la reputacion reflejo de el comportaiento del nodo? Realmente no se puede saber, es por ello que se utiliza una nueva metrica, el riesgo, que tiene valores entre 0 y 1 (menor a mayor riesgo).

#### Riesgo tipo 1 (Historial)

Si no se tienen las recomendaciones suficientes (ej: m recomendaciones), el riesgo de historial, es calculado como:

``` pseudo
J_1(x) = (1 - r/m)
//r : cantidad de recomendaciones que se pudieron obtener, con r < m
```

#### Riesgo tipo 2 (Variabilidad de recomendaciones)

```pseudo
J_2(x) = 4*suma(recomendacion(i)-promedio_recomendacion)^2/(n_total_recomendaciones)
//Varianza de las recomendaciones
```

### Uso del riesgo

Se suele hacer un calculo de el riesgo general, usando un factor ß

```pseudo
J(X) = ß*J_1(X) + (1-ß)*(J_2(X))

R(X) && J(X) => evaluacion de reputacion
```

## Anillo de confianza

Si el sistema tiene una cantidad >30% de actividad maliciosa, entonces se tiene una red inestable, para ello, se genera un anillo de confianza.
El anillo de confianza corresponde a una subred dentro del anillo de la red, obteniendose una red de nodo honestos (calificados segun un umbral de reputacion) y que a su vez, comparten las caracteristicas de una red estructurada, por lo tanto:

- El ruteo es de confianza
- Se llega a un nodo confiable
- Muy pocos fallos en el ruteo
- Menos dispositivos en el ruteo => menos saltos
- Cada LeafSet es un TrustedSet, donde todos los nodos son de confianza
