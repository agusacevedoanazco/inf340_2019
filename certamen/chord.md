# Chord

Al igual que pastry, es una red P2P, estructurada, del tipo de tablahash distribuida, su graficacion es de forma de anillo y su id se calcula como el Hash de la direccion publica Ip del peer.

## Informacion del nodo

El nodo guarda informacion de quien es su sucesor y su antecesor

```java
class chord_node{
    //atributes
    chord_node successor;
    chord_node predecessor;
}
```

## Ruteo de un dato

A diferencia de Pastry, el ruteo obtiene el nodo sucesor del hash del dato, en cambio en pastry, se obtenia el nodo numericamente más cercano al valor del hash del dato.

## Construccion del anillo

```pseudo
//Creacion de un anillo chord
n.create()
    predecessor = null;
    successor = n;
```

```pseudo
//Ingreso de un nodo n, conociendo el nodo n'
n.join(n')
    predecessor = null;
    successor = n'.find_successor(n);
```

En Chord no existe una noción de LeafSet, pero se tiene que un cierto numero de vecinos del nodo (definido como parametro de la red), manejan una copia del dato de la raiz.
A diferencia de Pastry. No hay balanceo de cargas, ya que todas las peticiones, llegan al nodo raiz.

## Actualizacion del anillo

```pseudo
//llamada periodicamente, para verificar el sucesor inmediato del nodo
//e informar de la existencia al sucesor en caso de que este no lo conozca
n.stabilize()
    x = successor.predecessor;
    if (x within ]n,successor[ )
        successor = x;
    successor.notify(n);
```

```pseudo
//n' always think it might be the predecessor
n.notify(n')
    if (predecessor == null || n' within ]predecessor,n[)
        predecessor = n';
```

Dado que stabilize() es llamado cada cierto tiempo, se tiene que el anillo se va constantemente actualizando, respecto al ingreso de nuevos nodos a la red, ya que estos ingresan de forma uniformemente distribuida a la red y por tanto, pueden ingresar en posiciones donde modifiquen el sucesor o antecesor del nodo raiz.

### Reactividad de la red

Dado que la red solo se actualiza en tiempos fijos (cada cierto ðt), ésta es menos reactiva que la red Pastry, la cual se actualiza en fixed time y cuando se notifica la desconexion.

## Tabla de ruteo

Cada nodo contiene un arreglo finger, que contiene el indice de los sucesores de los nodos, este se configura de la siguiente forma (finger de nodo A)

|indice |nodo   |
|-------|-------|
|2^0    |succ(A)|
|2^1    |...    |
|...    |...    |
|2^(k-1)|   Z   |

Donde Z = succ(nodeID(A)+2^(k-1)). Por lo tanto, cada nodo, conoce una cantidad k de nodos

La tabla se actualiza mediante las funciones

```pseudo
//llamada periodicamente, next es una variable estatica global
n.fix_finger()
    next = next + 1;
    if(next > k) next = 1;
    finger[next] = find_successor(n+2^(next-1));

//llamada periodicamente, verifica si el antecesor ha fallado
n.check_predecessor()
    if (predecessor has failed)
        predecessor = null;
```

## Chord Routing Algorithm

```pseudo
//pregunta al nodo n que encuentre el sucesor de id
n.find_successor(id)
    if (id within ]n,successor])
        return successor;
    else
        z = closest_preceding_node(id);
        return z.find_successor(id);

//busca en la tabla local, el predecesor mas alto para el id
n.closest_preceding_node(id)
    for i = k down to 1
        if finger[i] within [n,id]
            return finger[i]
    return n;
```

## Diferencias Chord y Pastry

- Chord es proactivo, cada cierto tiempo se repara el arreglo finger, se elimininan los nodos de la red y se agregan nodo nuevos en las casillas nuevas
- Tiene un politica pesimista, por lo tanto es más costoso y menos eficiente
- Pastry tiene una politica optimista, por tanto es menos costoso y más eficiente
