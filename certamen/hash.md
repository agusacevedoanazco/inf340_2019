# Funciones Hash

Es una funcion que lleva un conjunto de datos infinitos a un espacio de datos de cardinalidad definida.

## Teoremas

### No existe una funcion inversa

### Para cada elemento, existe un unico valor de hash

### Si dos valores son numericamente cercanos, sus valores de hash son numericamente lejanos

### Dos valores similares de hash provienen de valores numericamente lejanos

### Si se tiene un espacio de datos infinitos y se le aplica una función hash, puede darse la posibilidad de que los valores de hash sean iguales

### Es computacionalmente imposible obtener dos valores de hash iguales a partir de valores iniciales distintos

## Propiedades

Las funciones hash están uniformemente distribuidas y se les llaman funciones de hash fuerte. En caso contrario, son funciones hash debiles.
