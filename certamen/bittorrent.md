# BitTorrent

- Sistema no estructurado
- Bastante rapido
- No tiene algoritmo de busqueda, por lo cual la indexacion es externa (.web -> .torrent -> conexion a servidor tracker)
- Más anonima que un DHT
- Los datos son segmentados en trozos

Se vuelve a utilizar la clasificacion

- **Seeder**: Nodo que tiene todo el dato
- **Leecher**: Nodo que tiene solo una parte

**Objetivos**: maxima cantidad de seeer -> aumentar la disponibilidad de los datos

## Que contiene un dato .torrent

- Nombre del archivo
- Hash del dato
- Numero de segmento del dato
- Hash de cada segmento
- Tamaño total del archivo
- Lista de servidores trackers
- Otros datos

## Funcionamiento

Cuando un peer se conecta a un tracker, este transmite cuales son los trozos que tiene disponible para compartir y cuales le falta. El tracker decide compartir con él el trozo menos disponible del dato.
Para cada peer, el tracker calcula la tasa de bajada (T_b) y la tasa de subida (T_s). Si un peer tiene una alta tasa de bajada, implica que podría ser un buen contribuyente a la red, por lo cual le da prioridad para entregarle los datos menos disponibles que le faltan. A su vez, una alta tasa de subida, implica una gran capacidad de compartir datos y por tanto, tambien se le da prioridad a la entrega de los segmentos faltantes.

La indexacion de los datos, no asegura que estén disponibles (los trackers no tienen los datos, solo hacen de "puente" entre los peers).
Los trackers no se conocen entre sí.

## Aplicaciones de uso

El uso es limitado, por lo tanto se suele usar para el intercambio de datos.

### Problemas

Dado que el uso es limitado y depende unicamente de los peers conectados, la disponibilidad de los datos no es asegurada y depende de la popularidad de estos.
No tiene propiedades matemáticas fuertes.