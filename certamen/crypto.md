# Crypto

## Encriptacion simetrica

Se usa una funcion de encriptacion "E" y una funcion inversa de desencriptacion "E^(-1)", con una clave de encriptacion de modo que

```pseudo
m = mensaje
k = clave de encriptacion
m' = E(k,m)
m = E^(-1)(k,m')
```

Algoritmos conocidos de encriptacion simetrica son AED, 3DES, Blowfish y International Data Encryption Algorithm

### Ventajas SE

- Simplicidad
- Usado cuando se tiene un secreto compartido

### Desventajas SE

- Si se sabe la clave, se puede desencriptar facilmente
- Es un esquema rapido e inseguro

## Encriptacion Asimetrica

Se usan 2 claves para encriptar y desencriptar de modo que

```pseudo
k1 = clave 1
k2 = clave 2
k1 != k2
//E : funcion de encriptacion
m' = E(m,k1) => m = E(m',k2)
m' = E(m,k2) => m = E(m',k1)
```

Por lo general se tiene que una es una llave privada y la otra es una llave publica.

Algoritmos conocidos son: RSA, DSA, TLS, SSL, PGP.

### Ventajas AE

- Tiene mayor seguridad, dado que se suele compartir publicamente la llave publica, pero la llave privada se mantiene confidencial.

### Desventajas

- Mayor costo de CPU que en encriptacion simetrica

## Firmas digitales

Permiten autentificar la identidad de un usuario. Por lo cual, se usa que para mandar mensajes de un usuario a otro se une la firma digital, encriptada con la llave privada del emisor, con la informacion que se quiere enviar encriptada con la llave publica del remitente. Con lo cual se consigue que se verifique la identidad de emisor y que solo el remitente pueda leer la información enviada.