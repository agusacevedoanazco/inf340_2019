# Gnutella

- NO es una red estructurada
- Cada nodo tiene entre 15 a 30 vecinos
- Hay una cantidad inmensa de nodos
- El usuario ingresa a la red, consultando con un servidor, el cual le entrega un vecino para conectarse con la red
- El servidor de consultas puede generar cuellos de botella

Para **compartir datos**
El usuario que tenga el dato, debe almacenarlo en una carpeta compartida. Para que un cliente acceda al dato, este debe buscar el nodo que tenga en su carpeta compartida el dato.

Para ello se tienen varios algoritmos de busqueda.

## Algoritmo de busqueda de nodos Gnutella V1

### Flooding

- Se inunda la red de mensajes hasta encontrar el nodo que tenga el dato.
- Para evitar el colapso de la red, se agrega un campo TTL al mensaje de busqueda (algo así como un mensaje broadcast con tiempo de vida)
- Cada nodo le va quitando tiempo de vida (TTL) al mensaje. Si el nodo descubre que TTL es cero, descarta el mensaje

#### Ventajas flooding

Asegura que se va a encontrar una respuesta y todas las opciones disponibles

#### Deseventajas flooding

Muy costoso, demasiados mensajes en la red y se tienen muchas bsuquedas al mismo tiempo.

### RandomWalk

En vez de enviar el mensaje de busqueda a todos los "vecinos" se le envia a uno aleatoriamente hasta que TTL sea cero. La cantidad de mensajes será entonces TTL, por tanto TTL debe ser grande. La probabilidad de encontrar el dato es bajisima (más aun en redes muy grandes)

### K-RandomWalk

Se eligen k vecinos, donde k es menor que la cantidad máxima de vecinos de cada nodo. Se definen k y TTL parámetros fijos. Se tiene un compromiso entre k y TTL.

- Si k es muy grande, se acerca mucho al flooding, por lo tanto hay mayor alcance, pero se tiene una sobrecarga de la red con mensajes de busqueda.
- Si TTL es muy grande, se puede visitar la red con más profundidad, pero se tiene de igual forma una sobre carga de la red.

#### Ventajas K-RW

- Tiene mayor alcance que RandomWalk básico.
- Tiene mayor probabilidad de encontrar el dato buscado.

#### Desventajas K-RW

No se aprovecha la busqueda anterior

#### Lo cual lleva al algoritmo usado en esta versión

Dado que no se aprovecha la busqueda anterior, se decide agregar informacion en la carpeta compartida, acerca de las busquedas anteriores.

### K-RandomWalk + caché

Se mejora en terminos de costo de mensajes, mejor presencia y por tanto mayor probabilidad de encontrar el dato.

## Respecto a Gnutella V1

1) A pesar de que la busqueda se ve optimizada, se desea tener más copias del dato, para conseguir una mayor probabilidad de busqueda
2) Se desea visitar mas nodo, sin alterar la carga de la red
3) Se desea encontrar el equilibrio justo entre el costo/disponibilidad

### Problema de las redes estructuradas

- El usuario decide que compartir, por lo tanto, depende de su comportamiento respecto a la carpeta compartida.
- No se tiene control de la disponibilidad ya que ésta depende del comportamiento del usuario

## Gnutella V2

Se crea un grafo de super-nodos que se comunican entre ellos y a su vez están conectados a nodos "menores". Los nodos conectados al super nodo, le comunican cuales son los datos que tienen para compartir, por lo cual, se tiene mayor profundidad virtualmente, ya que cada supernodo, se comporta como un nodo con los datos de sus "nodos hijos" y la busqueda sería solo entre estos.

## Problema de Gnutella

Como se dijo anteriormente, el usuario decide, por lo que se puede calificar al usuario como:

- Seeder: Nodo que tiene el dato completo para compartir
- Leecher: Nodo que está bajando el dato, no tiene el dato completo y/o tiene el dato completo, pero lo eliminó de su carpeta compartida.

Se suele tener que hay muuuuuchos mas Leechers que Seeders (2% de los nodos tienen el 98% de los datos que se buscan). Por lo tanto NO HAY balance de cargas.
Por lo tanto, a dia de hoy la aplicacion de Gnutella es casi nula (ej: nadie usa eMule, uno de las aplicaciones mas conocidas de una red Gnutella)