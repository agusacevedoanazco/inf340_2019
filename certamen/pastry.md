# Pastry

Es un tipo de red P2P estructurada. Basada en la función de hashing SHA-1 (160 bits). Cada peer es identificado mediante el hash de se direccion ip publica **NodeID = SHA-1(@pubIP)**.

## Propiedades

- Cada nodo tiene un LeafSet, correspondiente a los hash de direcciones de los peers vecinos. El tamaño del Leafset es un parámetro fijo de la red.
- Cada Leafset contiene vecinos con valores de Hash cercano, lo cual implica que se encuentran geograficamente lejanos.
- Dado que los vecinos son geograficamente lejanos, la probabilidad de que estos nodos se "caigan" de manera simultanea es baja.
- El nodo encargado de un dato, nodo raiz, copia los datos en todo su LeafSet, por lo cual se tiene redundancia de los datos a nivel "global" y por lo tanto, las copias del dato resisten cualquier particion de red.

## Si un nodo se desconecta

En el caso de que un nodo se desconecta, se tiene que mantener el LeafSet del nodo raiz, por lo que la red, genera las copias en el nuevo nodo más cercano dentro del LeafSet que mantenga sus propiedades. Pueden ocurrir dos tipos de desconexiones:

- Desconexion avisada: El nodo que se desconecta, se retira avisando con un mensaje "leave", por lo cual, el nodo que se encuentre contiguo al que se retira, es el encargado de presentar al nuevo candidato para ingresar al LeafSet.
- Desconexion subita: La red está preparada para las caidas repentinas de los nodos dentro del LeafSet, mediante el envío de parte del nodo raiz de un mensaje "keep_alive" cada cierto tiempo. Este tiempo debe estar en compromiso de que sea lo suficientemente pequeño para que la red sea lo más reactiva posible y lo suficientemente grande, para evitar la inundación de la red.

## Algoritmo de Ruteo

El algoritmo de ruteo, se encarga de balancear la carga de los peers a lo largo de todo el LeafSet, de modo que ningun nodo sea sobrecargado respecto a los demás.

### Informacion de ruteo

Cada nodo maneja la siguiente informacion: Su *leafSet*, que contiene sus L nodos numericamente mas cercanos; una *tabla de ruteo*, usada por el algoritmo de ruteo para obtener la ruta hacia la llave k y un *set de vecinos* que representan los nodos mas cercanos usando alguna metrica (usalmente la metrica es la latencia).

### Pastry Routing Algorithm

Se desea enviar un mensaje con llave k
Se si el valor al cual se desea rutear se encuentra entre los limites del LeafSet, si es así, se transmite el mensaje al nodo con la diferencia entre el Leafset (L) y la llave (k) menor.
Si no se encuentra el valor en el LeafSet, entonces se utiliza la tabla de ruteo. Para ello se compara a partir de los bits más significativos congruentes y se hace un forward a la direccion que se obtenga. Por ejemplo, si se tiene que se desea rutear un mensaje con llave FFA5 y se parte desde el nodo FFB3, suponiendo que FFB3 cumple la primera condicion, se usa la tabla de ruteo y se hace un forward del mensaje a la direccion correspondiente a FFAX, luego el nodo que recibe realiza la misma operacion y así sucesivamente hasta llegar al nodo numericamente más cercano a la llave k.

## Problemas de Pastry

### Si un archivo es muy demandado

Una solucion sería agrandar el LeafSet, pero habría que modificar completamente la red, solucion muy costosa.
La solucion suele ser hacer una Hash recursivo del dato, de modo de tener un nivel más de copias del dato, teniendo el nodo raiz de nivel 1 en el nodo mas cercano a Hash("-"), luego el nodo raiz de nivel dos en el nodo mas carcano a Hash(Hash("-")) y así sucesivamente, obteniendose un total de k niveles y por tanto, obteniendo un total de k*(L+1) copias.
Esto conlleva a que se deba guardar informacion acerca de la cantidad de niveles del dato.

### Quien decide aumentar el numero de niveles

El nodo raiz, quien superado un umbral de peticiones por minuto, genera un nuevo nivel y en caso contrario, si se ve que hay una baja demanda, se eliminan los niveles.

### Calculo estimado de nodos

Dado que el sistema es uniforme, se realiza una aproximacion de la cantidad de nodos.
