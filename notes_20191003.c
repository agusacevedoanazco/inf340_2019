/******************************************************************************
Cluster/Grids:
- Homogeneidad del hardware.
- Conexion Internet -> High Gb/sec.
                    -> Simetric BW.
- Administración es centralizada.
- Se estima que Google tiene alrededor de 10**6 servidores.
******************************************************************************/

/******************************************************************************
P2P:
- El hardware no es homogeneo
- Velocidades del orden de los Mbps -> Velocidades asimetricas
                                    -> BW bajada >>> BW subida => Como cliente
                                       es muy bueno, pero si quiero actuar como
                                       servidor, el QOS de mis clientes será 
                                       pauperrima
- La administración no es centralizada, nadie sabe como se comportará el
  usuario dentro del computador. Nadie puede imponer el comportamiento de la
  red.
- En el caso de un usuario envía datos involuntariamente (ej: tiene un virus),
  es muy dificil controlar esto, ya que no se tiene un control centralizado ni
  normalizado del comportamiento de este.
- Cada equipo puede tener ambos roles, cliente-servidor.
- Una red P2P suele contener n*10**6 servidores.
- En una red P2P, es muy sencillo "mentir". => De ahi nacen los sistemas de
  reputación de usuarios.
******************************************************************************/

/******************************************************************************
La mayoria de los algoritmos utilizados para resolver las problematicas en
cluster/grids, no son aplicables en redes P2P, ya que se utiliza sobre redes
no controladas.
******************************************************************************/

/******************************************************************************
Hay dos tipos de redes P2P:
    1. Redes P2P Estructuradas      : Tienen propiedades matematicamente fuerte
    2. Redes P2P No Estructuradas   : NO tienen prop. matematicamente fuertes

En redes estructuradas se tienen: (no son anonimas)
    - Pastry            - Será visto
    - Chord             - Será visto
    - Can
    - Tapestry
    - Kadenlia
    - Viceroy
    - Symphony
Permite hacer muchos tipos de aplicaciones.

En redes no estructuradas: (imposible de localizar)
    - Kaaza
    - Edonkey
    - Gnutella (Emule)  - Será visto
    - BitTorrent        - Será visto
Permite practicamente solo compartir archivos.
******************************************************************************/

/******************************************************************************
PASTRY
- Está basado en SHA-1 : 160 bits

Cada computador es identificado por una dirección IP pública => se calculará
al SHA-1(@pubIP). Por lo tanto, el valor Hash Calculado, corresponderá a:
"Node_ID de X", con X la dirección IP publica del computador. Los Hash vecinos,
corresponderán al LeafSet de tamaño L => a la izquierda se tienen L/2 vecinos y
a la derecha L/2 vecinos (Usualmente en Pastry, se suele usar 16, 32, 64, etc.)
En cada LeafSet, los valores de Hash son cercanos (porque son vecinos),
entonces las direcciones IP son muy distintas (probablemente). Lo cual implica,
que fisicamente, los computadores asociados a estas direcciones se encuentran
en lugares muy distintos (o incluso lejanos).

La "gracia" de los LeafSet es:
    - Muy poco probable que los nodos dentro de este se caigan
    - Si un dato es almacenado en un nodo, el nodo, copia los datos, en todos
    los nodos de su leafset y además se dice que: el nodo es la raiz del dato.
    Por lo tanto se tiene L+1 copias del dato => Las copias se encuentran 
    almacenadas en nodos muy lejanos entre si. Lo cual genera que el dato es
    muy poco probable se pierda. Se puede suponer que las copias del dato,
    resisten cualquier particion de red.
******************************************************************************/
#include <stdio.h>

void main(void){
    printf("Notas del día 03 de Octubre de 2019 de Computación Peer to Peer, ver archivo fuente.\n");
}